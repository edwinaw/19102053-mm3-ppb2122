package com.edwin_19102053.lifecycleapp

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.edwin_19102053.lifecycleapp.databinding.ActivityHalamanDuaBinding

class HalamanDua : AppCompatActivity() {

    private lateinit var binding: ActivityHalamanDuaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHalamanDuaBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
    private fun printState(msg: String){
        Log.d("ActivityState",msg)
        Toast.makeText(applicationContext,msg, Toast.LENGTH_SHORT).show()
    }
    override fun onStart(){
        super.onStart()
        printState("Halaman dua On Start")
    }
    override fun onResume(){
        super.onResume()
        printState("Halaman dua On Resume")
    }
    override fun onPause(){
        super.onPause()
        printState("Halaman dua On Pause")
    }
    override fun onStop(){
        super.onStop()
        printState("Halaman dua On Stop")
    }
    override fun onRestart(){
        super.onRestart()
        printState("Halaman dua On Restart")
    }
    override fun onDestroy() {
        super.onDestroy()
        printState("Halaman dua On Destroy")
    }
}
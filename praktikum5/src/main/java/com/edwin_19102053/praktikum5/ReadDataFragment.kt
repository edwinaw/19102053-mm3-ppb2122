package com.edwin_19102053.praktikum5

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.edwin_19102053.praktikum5.databinding.FragmentReadDataBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "extra_nama"
private const val ARG_PARAM2 = "extra_nim"

/**
 * A simple [Fragment] subclass.
 * Use the [ReadDataFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReadDataFragment : Fragment() {
    private var _binding: FragmentReadDataBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentReadDataBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Using this with two var on comapnion object i got null i dont know why.
//        if (arguments != null) {
//            val nama = arguments?.getString(EXTRA_NAMA)
//            val nim = arguments?.getInt(EXTRA_NIM)
//            binding.textViewData.text = "Nama Saya : $nama, NIM Saya : $nim"
//        }
        arguments?.let {
            val text = "Nama Saya : ${it.getString(ARG_PARAM1)}, NIM Saya : ${it.getInt(ARG_PARAM2)}"
            binding.textViewData.text = text
        }

        binding.buttonBackHome.setOnClickListener{
            val mIntent = Intent(activity, Practice5Activity::class.java)
            startActivity(mIntent)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param nama Parameter 1.
         * @param nim Parameter 2.
         * @return A new instance of fragment ReadDataFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(nama: String, nim: Int) =
            ReadDataFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, nama)
                    putInt(ARG_PARAM2, nim)
                }
            }

        // I got null when i apply
//        var EXTRA_NAMA = ""
//        var EXTRA_NIM = ""
    }
}
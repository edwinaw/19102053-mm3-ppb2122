package com.edwin_19102053.praktikum14.model

data class QuoteResponse(
    val quotes: ArrayList<Quote>)
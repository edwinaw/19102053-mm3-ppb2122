package com.edwin_19102053.praktikum9

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SettingModel (
    var name: String? = null,
    var email: String? = null,
    var age: Int = 0,
    var phoneNumber: String? = null,
    var isDarkTheme: Boolean = false,
    var quote1: String? = null,
    var quote2: String? = null
): Parcelable
package com.edwin_19102053.praktikum8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class SmsReceiverActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sms_receiver)

        title = getString(R.string.incoming_message)
        val senderNo = intent.getStringExtra(EXTRA_SMS_NO)
        val senderMessage = intent.getStringExtra(EXTRA_SMS_MESSAGE)

        val tv_from = findViewById<TextView>(R.id.textView)
        val tv_message = findViewById<TextView>(R.id.textView2)
        val btn_close = findViewById<Button>(R.id.button2)

        tv_from.text = getString(R.string.coming_from)+": "+senderNo
        tv_message.text = checkMessage(senderMessage.toString())
        btn_close.setOnClickListener { finish() }
    }

    fun checkMessage(senderMessage: String): String {
        val penipuan = "hadiah,  blogspot,  wordpress,  pulsa,  selamat, transfer, mobil, polisi, rumah"
            .split(",", " ")
        val ssenderMessage = senderMessage.split(",", " ")

        val adaPenipu = ssenderMessage intersect penipuan
        if(adaPenipu.isNotEmpty()) {
//            Toast.makeText(this, "Sms di deteksi penipuan.", Toast.LENGTH_SHORT).show()
            return "!Sms di deteksi penipuan.! \n".plus(senderMessage)
        }

        return senderMessage
    }

    companion object {
        const val EXTRA_SMS_NO = "extra_sms_no"
        const val EXTRA_SMS_MESSAGE = "extra_sms_message"
    }
}
package com.edwin_19102053.praktikum6.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.edwin_19102053.praktikum6.MyData
import com.edwin_19102053.praktikum6.R
import com.bumptech.glide.request.RequestOptions

class GridMyDataAdapter(private val listMyData: ArrayList<MyData>):
    RecyclerView.Adapter<GridMyDataAdapter.GridViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GridViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_grid, viewGroup, false)
        return GridViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        holder.bind(listMyData[position])
    }
    override fun getItemCount(): Int = listMyData.size

    class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img_item_photo: ImageView

        init {
            img_item_photo = itemView.findViewById(R.id.img_item_photo)
        }

        fun bind(myData: MyData) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(myData.photo)
                    .apply(RequestOptions().override(350, 550))
                    .into(img_item_photo)
            }
        }
    }
}
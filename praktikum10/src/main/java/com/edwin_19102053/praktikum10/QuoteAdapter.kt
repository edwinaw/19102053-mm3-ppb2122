package com.edwin_19102053.praktikum10

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.edwin_19102053.praktikum10.databinding.ItemQuoteBinding
import com.edwin_19102053.praktikum10.helper.EXTRA_POSITION
import com.edwin_19102053.praktikum10.helper.EXTRA_QUOTE
import com.edwin_19102053.praktikum10.helper.REQUEST_UPDATE
import com.edwin_19102053.praktikum10.helper.categoryList

class QuoteAdapter(private val activity: Activity):
    RecyclerView.Adapter<QuoteAdapter.QuoteViewHolder>() {
    var listQuotes = ArrayList<Quote>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            QuoteViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_quote, parent, false)
        return QuoteViewHolder(view)
    }
    override fun getItemCount(): Int = this.listQuotes.size
    override fun onBindViewHolder(holder: QuoteViewHolder, position: Int) {
        holder.bind(listQuotes[position],position)
    }
    inner class QuoteViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val binding = ItemQuoteBinding.bind(itemView)
        fun bind(quote: Quote, position: Int) {
            binding.tvItemTitle.text = quote.title
            binding.tvItemCategory.text = categoryList[quote.category!!.toInt()]
            binding.tvItemDate.text = quote.date
            binding.tvItemDescription.text = quote.description
            binding.tvItemAuthor.text = quote.author
            binding.cvItemQuote.setOnLongClickListener{
                val intent = Intent(activity, QuoteAddUpdateActivity::class.java)
                intent.putExtra(EXTRA_POSITION, position)
                intent.putExtra(EXTRA_QUOTE, quote)
                activity.startActivityForResult(intent, REQUEST_UPDATE)
                true
            }
            binding.cvItemQuote.setOnClickListener{
                if (Patterns.WEB_URL.matcher(quote.url).matches()) {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(quote.url)
                    activity.startActivity(intent)
                } else {
                    Toast.makeText(itemView.context, "No URL.", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun addItem(quote: Quote) {
        this.listQuotes.add(quote)
        notifyItemInserted(this.listQuotes.size - 1)
    }
    fun updateItem(position: Int, quote: Quote) {
        this.listQuotes[position] = quote
        notifyItemChanged(position, quote)
    }
    fun removeItem(position: Int) {
        this.listQuotes.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, this.listQuotes.size)
    }
}
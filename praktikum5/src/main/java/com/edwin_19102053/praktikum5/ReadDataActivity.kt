package com.edwin_19102053.praktikum5

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.edwin_19102053.praktikum5.databinding.ActivityReadDataBinding

class ReadDataActivity : AppCompatActivity() {
    private lateinit var binding: ActivityReadDataBinding
    companion object {
        const val EXTRA_INPUT = "extra_input"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val input = intent.getStringExtra(EXTRA_INPUT)
        binding.textView2.text = "Input Adalah $input"
    }
}
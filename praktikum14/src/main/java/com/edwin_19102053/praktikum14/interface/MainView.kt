package com.edwin_19102053.praktikum14.`interface`

import com.edwin_19102053.praktikum14.model.Login
import com.edwin_19102053.praktikum14.model.Quote

interface MainView {
    fun showMessage(messsage : String)
    fun resultQuote(data: ArrayList<Quote>)
    fun resultLogin(data: Login)
}
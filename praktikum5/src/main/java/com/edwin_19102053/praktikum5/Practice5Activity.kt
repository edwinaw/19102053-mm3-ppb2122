package com.edwin_19102053.praktikum5

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.edwin_19102053.praktikum5.databinding.ActivityPractice5Binding

class Practice5Activity : AppCompatActivity() {
    private lateinit var binding: ActivityPractice5Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPractice5Binding.inflate(layoutInflater)
        setContentView(binding.root)

        setupPermissions()

        binding.buttonSend.setOnClickListener {
            val input = binding.textInputEditText
            if(input.text.toString().isEmpty()) {
                input.error = "Input Tidak Boleh Kosong!"
                return@setOnClickListener
            }

            val moveWithDataIntent = Intent(binding.root.context, ReadDataActivity::class.java)
            moveWithDataIntent.putExtra(ReadDataActivity.EXTRA_INPUT, input.text.toString())
            startActivity(moveWithDataIntent)
        }

        binding.buttonBrowser.setOnClickListener {
            val url = binding.editTextUrl.text
            if (url.isEmpty()) {
                binding.editTextUrl.error = "Url Tidak Boleh Kosong"
                return@setOnClickListener
            }

            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url.toString())
            startActivity(intent)
        }

        binding.buttonCamera.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivity(intent)
        }

        binding.buttonCallPhone.setOnClickListener {
            val phoneNumber = binding.editTextPhoneNumber.text
            if (phoneNumber.isEmpty()) {
                binding.editTextPhoneNumber.error = "Nomor Telpon Tidak Boleh Kosong"
                return@setOnClickListener
            }
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:$phoneNumber")
            startActivity(intent)
        }

        binding.buttonFragment.setOnClickListener {
            val intent = Intent(this, ForFragmentActivity::class.java)
            startActivity(intent)
        }
    }

    val CALL_REQUEST_CODE = 100
    @SuppressLint("MissingPermision")
    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.CALL_PHONE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CALL_PHONE),
                CALL_REQUEST_CODE)
        }
    }
}